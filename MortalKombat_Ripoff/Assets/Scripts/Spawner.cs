﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject noob;
   // public GameObject noob2;
    public GameObject kabal;
    //public GameObject kabal2;

    public GameObject player;


    private void Awake()
    {
        if (this.gameObject.name == "P1Spawner")
        {
            if (GameManager.Instance.p1 == "Noob")
            {
                player = Instantiate(noob, transform.position, Quaternion.identity);
                player.gameObject.name = "Noob";
            }
            else if (GameManager.Instance.p1 == "Kabal")
            {
                player = Instantiate(kabal, transform.position, Quaternion.identity);
                player.gameObject.name = "Kabal";
            }
            player.gameObject.GetComponent<PlayerMovement>().Player = 1;
        }
        else if (this.gameObject.name == "P2Spawner")
        {
            if (GameManager.Instance.p2 == "Noob")
            {
                player = Instantiate(noob, transform.position, Quaternion.identity);
                player.gameObject.name = "Noob";
            }
            else if(GameManager.Instance.p2 == "Kabal")
            {
                player = Instantiate(kabal, transform.position, Quaternion.identity);
                player.gameObject.name = "Kabal";
            }
            player.gameObject.GetComponent<PlayerMovement>().Player = 2;
        }
        
        
    }

    public void respawn()
    {
        player.transform.position = this.transform.position;
    }
}
