﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour
{
    GameObject target;
    Transform targetPos;
    float speed = 6;
    Rigidbody2D _rb;
    public int player;

    // Start is called before the first frame update
    void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
        if (this.player == 1)
        {
            target = GameObject.Find("P2Spawner").gameObject.GetComponent<Spawner>().player;
        }
        else if (this.player == 2)
        {
            target = GameObject.Find("P1Spawner").gameObject.GetComponent<Spawner>().player;
        }
        targetPos = target.transform;
        Destroy(this.gameObject, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (targetPos.position.x < this.transform.position.x)
        {
             _rb.velocity = new Vector3(-speed, _rb.velocity.y);
        }
        else
        {
            _rb.velocity = new Vector3(speed, _rb.velocity.y); ;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<LifeManager>().animator.GetBool("block"))
            {
                collision.gameObject.GetComponent<LifeManager>().getHit(2);
            }
            else
            {
                collision.gameObject.GetComponent<LifeManager>().getHit(4);
            }
            Destroy(this.gameObject, 1f);
        }
    }
}
