﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KabalAttacks : MonoBehaviour
{
    public GameObject character;
    public GameObject special;
    public Transform shootPlace; 

    Animator _animator;
    int comboStatus;
    int comboStatusA;
    int player;


    // Start is called before the first frame update
    void Start()
    {
        player = this.gameObject.GetComponent<PlayerMovement>().Player;
        _animator = character.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.isGameOver)
        {
            attack();
            block();
        }

    }

    private void block()
    {
        if (player == 1)
        {
            if (Input.GetKey(KeyCode.L))
            {
                _animator.SetBool("block", true);
            }
            else if (!Input.GetKey(KeyCode.S))
            {
                _animator.SetBool("block", false);
                
            }
        }
        else if (player == 2)
        {
            if (Input.GetKey(KeyCode.Keypad6))
            {
                _animator.SetBool("block", true);
            }
            else if (!Input.GetKey("down"))
            {
                _animator.SetBool("block", false);
            }
        }
    }

    private void attack()
    {
        if (player == 1)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                StartCoroutine(combo1("s"));
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                _animator.SetTrigger("upKick");
                StartCoroutine(combo("k"));
                StartCoroutine(combo1("k"));
            }
            if (Input.GetKeyDown(KeyCode.J))
            {
                _animator.SetTrigger("punchUp");
                StartCoroutine(combo("p"));
                StartCoroutine(combo1("p"));
            }
        }
        else if (player == 2)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                StartCoroutine(combo1("s"));
            }
            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                _animator.SetTrigger("upKick");
                StartCoroutine(combo("k"));
                StartCoroutine(combo1("k"));
            }
            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                _animator.SetTrigger("punchUp");
                StartCoroutine(combo("p"));
                StartCoroutine(combo1("p"));
            }
        }
    }

    //p p k p
    IEnumerator combo(string key)
    {
        if (key == "k")
        {
            if (comboStatus == 2)
            {
                comboStatus = 3;
                yield return new WaitForSeconds(0.4f);
                if (comboStatus == 3)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
            }
        }
        else if (key == "p")
        {
            if (comboStatus == 0)
            {
                comboStatus = 1;
                yield return new WaitForSeconds(0.4f);
                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else if (comboStatus == 1)
            {
                comboStatus = 2;
                yield return new WaitForSeconds(0.4f);
                if (comboStatus == 2)
                {
                    comboStatus = 0;
                }
            }
            else if (comboStatus == 3)
            {
                comboStatus = 4;
                _animator.SetTrigger("special");
                Instantiate(special, shootPlace.position, Quaternion.identity).gameObject.GetComponent<Saw>().player = this.player;
            }
            else
            {
                comboStatus = 0;
            }
        }
    }
    // k s s p 
    IEnumerator combo1(string key)
    {
        if (key == "k")
        {
            if (comboStatusA == 0)
            {
                comboStatusA = 1;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusA == 1)
                {
                    comboStatusA = 0;
                }
            }
            else
            {
                comboStatusA = 0;
            }
        }
        else if (key == "s")
        {
            if (comboStatusA == 1)
            {
                comboStatusA = 2;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusA == 2)
                {
                    comboStatusA = 0;
                }
            }
            else if (comboStatusA == 2)
            {
                comboStatusA = 3;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusA == 3)
                {
                    comboStatusA = 0;
                }
            }
            else
            {
                comboStatusA = 0;
            }
        }
        else if (key == "p")
        {
            if (comboStatusA == 3)
            {
                comboStatusA = 4;
                _animator.SetTrigger("axe");
            }
            else
            {
                comboStatusA = 0;
            }
        }
    }
}
