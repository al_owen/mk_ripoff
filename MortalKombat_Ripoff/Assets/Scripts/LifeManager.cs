﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour
{
    public int life;
    public ParticleSystem bloodParticles;
    public Animator animator;

    public int Life { get => life; }

    public delegate void lifeDelegate();
    public event lifeDelegate lifeEvent;

    private void Start()
    {
        life = 100;
        animator = transform.GetChild(0).gameObject.GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "fullKick")
        {
            if (animator.GetBool("block"))
            {
                getHit(1);
            }
            else
            {
                getHit(2);
            } 
        }
        else if (collision.gameObject.name == "midPunch")
        {
            if (animator.GetBool("block"))
            {
                getHit(1);
            }
            else
            {
                getHit(3);
            }
        }
        else if (collision.gameObject.name == "fullPunch")
        {
            if (animator.GetBool("block"))
            {
                getHit(2);
            }
            else
            {
                getHit(4);
            }
        }

    }

    public void getHit(int damage)
    {
        if (life > 0 && !GameManager.Instance.isGameOver)
        {
            life -= damage;
            bloodParticles.Play();
            if (lifeEvent != null)
            {
                lifeEvent();
            }
        }
        else
        {
            life = 0;
        }

        
    }

    public void resetLife()
    {
        life = 100;
    }
}
