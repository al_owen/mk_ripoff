﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HealthBar: MonoBehaviour
{
    public string playerBar;
    public Text text;
    public Image image;

    public Sprite noob;
    public Sprite kabal;

    // Start is called before the first frame update
    void Start()
    {
        if (playerBar == "p1")
        {
            if (GameManager.Instance.p1 == "Noob")
            {
                text.text = "Noob Saibot";
                image.GetComponent<Image>().sprite = noob;
            }
            else if (GameManager.Instance.p1 == "Kabal")
            {
                text.text = "Kabal";
                image.GetComponent<Image>().sprite = kabal;
            }
        }
        else if (playerBar == "p2")
        {
            if (GameManager.Instance.p2 == "Noob")
            {
                text.text = "Noob Saibot";
                image.GetComponent<Image>().sprite = noob;
            }
            else if (GameManager.Instance.p2 == "Kabal")
            {
                text.text = "Kabal";
                image.GetComponent<Image>().sprite = kabal;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
