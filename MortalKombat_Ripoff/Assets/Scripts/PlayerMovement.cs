﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    string charname;
    public string Charname { get => charname; }

    int player;
    public int Player { get => player; set => player = value; }

    public float jumpForce;
    public float speed;
    public playerSide side;
    public CapsuleCollider2D standCollider;
    public Animator _animator;

    GameObject character;
    private bool _canMove;
    bool isCrouching;
    bool isGrounded;
    GameObject target;
    Rigidbody2D _rb;
    float _horizontal;
    bool movingRight;
    bool movingLeft;

    // Start is called before the first frame update
    void Start()
    {
        _canMove = true;
        charname = this.gameObject.name;
        if (this.player == 1)
        {
            target = GameObject.Find("P2Spawner").gameObject.GetComponent<Spawner>().player;
        }
        else if (this.player == 2)
        {
            target = GameObject.Find("P1Spawner").gameObject.GetComponent<Spawner>().player;
        }
        character = this.gameObject.transform.GetChild(0).gameObject;
        _animator = character.gameObject.GetComponent<Animator>();
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!GameManager.Instance.isGameOver)
        {
            if(this.player == 1)
            {
                _horizontal = Input.GetAxis("player1");
            }
            else if(this.player == 2)
            {
                _horizontal = Input.GetAxis("player2");
            }
            _animator.SetFloat("speed", Mathf.Abs(_horizontal)); //returns the absolute number

            jump();
            faceEnemy();
            crouch();
        }
        else
        {
            _rb.velocity = new Vector2(0f, _rb.velocity.y);
            _animator.SetFloat("speed", 0f);
        }
    }

    private void crouch()
    {
        if (this.player == 1)
        {
            if (Input.GetKey(KeyCode.S) && isGrounded)
            {
                doCrouch();
            }
            else if (!Input.GetKey(KeyCode.S))
            {
                unCrouch();
            }
        }
        else if(this.player == 2)
        {
            if (Input.GetKey("down") && isGrounded)
            {
                doCrouch();
            }
            else if (!Input.GetKey("down"))
            {
                unCrouch();
            }
        }
    }

    public void doCrouch()
    {
        standCollider.enabled = false;
        _animator.SetBool("isCrouching", true);
    }

    public void unCrouch()
    {
        standCollider.enabled = true;
        _animator.SetBool("isCrouching", false);
    }

    private void faceEnemy()
    {
        float myPosition = this.transform.position.x;
        float enemyPostion = target.transform.position.x;
        if ((myPosition < enemyPostion) && side == playerSide.left)
        {
            character.gameObject.transform.localScale *= new Vector2(-1, 1);
            side = playerSide.right;
        }
        else if ((myPosition > enemyPostion) && side == playerSide.right)
        {
            character.gameObject.transform.localScale *= new Vector2(-1, 1);
            side = playerSide.left;
        }
    }

    private void jump()
    {
        if (this.player == 1)
        {
            if (Input.GetButtonDown("Jump") && isGrounded && !_animator.GetBool("block"))
            {
                _rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
                _animator.SetBool("isJumping", true);
            }
        }
        else if (this.player == 2)
        {
            if (Input.GetKeyDown(KeyCode.Keypad0) && isGrounded && !_animator.GetBool("block"))
            {
                _rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
                _animator.SetBool("isJumping", true);
            }
        }
    }

    void FixedUpdate()
    {
        if (!_animator.GetBool("block") && _canMove)
        {
            _rb.velocity = new Vector2(_horizontal * speed, _rb.velocity.y);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "floor")
        {
            isGrounded = true;
            _animator.SetBool("isJumping", false);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "floor")
        {
            isGrounded = false;

        }
    }

    public void freeze()
    {
        _canMove = false;
        Invoke("unfreeze", 5f);
    }

    public void unfreeze()
    {
        _canMove = true;
    }
}

public enum playerSide
{
    left,
    right
}
