﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public bool isGameOver;
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public string p1;
    public string p2;

    private GameManager()
    {

    }

    void Awake()
    {
        isGameOver = false;
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }



    public void restart()
    {
        isGameOver = false;
    }
}
