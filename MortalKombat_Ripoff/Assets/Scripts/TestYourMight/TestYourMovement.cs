﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestYourMovement : MonoBehaviour
{

    public float jumpForce;
    public float speed;
    public CapsuleCollider2D standCollider;
    public Animator _animator;
    public playerTSide side;

    GameObject character;
    private bool _canMove;
    bool isCrouching;
    bool isGrounded;
    Rigidbody2D _rb;
    float _horizontal;
    bool movingRight;
    bool movingLeft;

    // Start is called before the first frame update
    void Start()
    {
        character = this.gameObject.transform.GetChild(0).gameObject;
        _animator = character.gameObject.GetComponent<Animator>();
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _horizontal = Input.GetAxis("player1");
        _animator.SetFloat("speed", Mathf.Abs(_horizontal)); //returns the absolute number

        jump();
        faceEnemy();
        crouch(); 
    }

    private void crouch()
    {
        if (Input.GetKey(KeyCode.S) && isGrounded)
        {
            doCrouch();
        }
        else if (!Input.GetKey(KeyCode.S))
        {
            unCrouch();
        } 
    }

    public void doCrouch()
    {
        standCollider.enabled = false;
        _animator.SetBool("isCrouching", true);
    }

    public void unCrouch()
    {
        standCollider.enabled = true;
        _animator.SetBool("isCrouching", false);
    }

    private void faceEnemy()
    {
        float myPosition = this.transform.position.x;
        if (Input.GetKey(KeyCode.D) && side == playerTSide.left)
        {
            character.gameObject.transform.localScale *= new Vector2(-1, 1);
            side = playerTSide.right;
        }
        else if (Input.GetKey(KeyCode.A) && side == playerTSide.right)
        {
            character.gameObject.transform.localScale *= new Vector2(-1, 1);
            side = playerTSide.left;
        }
    }

    private void jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded && !_animator.GetBool("block"))
        {
            _rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            _animator.SetBool("isJumping", true);
        }
        
    }

    void FixedUpdate()
    {
        if (!_animator.GetBool("block"))
        {
            _rb.velocity = new Vector2(_horizontal * speed, _rb.velocity.y);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "floor")
        {
            isGrounded = true;
            _animator.SetBool("isJumping", false);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "floor")
        {
            isGrounded = false;

        }
    }

    public void freeze()
    {
        _canMove = false;
        Invoke("unfreeze", 5f);
    }

    public void unfreeze()
    {
        _canMove = true;
    }

    public enum playerTSide
    {
        left,
        right
    }
}
