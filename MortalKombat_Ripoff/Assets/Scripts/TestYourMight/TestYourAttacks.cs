﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestYourAttacks : MonoBehaviour
{
    public GameObject character;

    Animator _animator;
    int comboStatus;


    // Start is called before the first frame update
    void Start()
    {
        _animator = character.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        attack();
        block();
    }

    private void block()
    {
        if (Input.GetKey(KeyCode.L))
        {
            _animator.SetBool("block", true);
        }
        else if (!Input.GetKey(KeyCode.S))
        {
            _animator.SetBool("block", false);
        }
    }

    private void attack()
    {    
        if (Input.GetKeyDown(KeyCode.K))
        {
            _animator.SetTrigger("upKick");
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            _animator.SetTrigger("punchUp");
        }   
    }


}
