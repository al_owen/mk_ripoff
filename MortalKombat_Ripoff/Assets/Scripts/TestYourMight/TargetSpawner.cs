﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    public GameObject coins;
    public int coinCounter;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        coinCounter = 20;
        InvokeRepeating("spawnCoins", 1f, 5f );
    }

    public void spawnCoins()
    {
        if (coinCounter > 0)
        {
            Instantiate(coins, transform).gameObject.GetComponent<Coin>().spawner = this.gameObject;
        }
        else
        {
            animator.SetTrigger("victory");
            Invoke("menu", 2f);
        }
    }
    public void menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
