﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject spawner;

    private void Start()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2( Random.Range(-5, 5) , this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "fullKick" || collision.gameObject.name == "midPunch")
        {
            spawner.gameObject.GetComponent<TargetSpawner>().coinCounter--;
            Destroy(this.gameObject);
        }
        
    }
}
