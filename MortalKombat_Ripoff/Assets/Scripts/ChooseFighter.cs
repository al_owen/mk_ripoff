﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class ChooseFighter : MonoBehaviour
{
    public Text select;
    public Text selectText;
    public GameObject scenarios;
    public GameObject characters;
    bool p1selected;
    bool p2selected;


    // Start is called before the first frame update
    void Start()
    {
        p1selected = false;
        p2selected = false;
    }

    public void fighterNoob()
    {
        if (!p1selected)
        {
            GameManager.Instance.p1 = "Noob";
            selectText.text = "Player 2";
            p1selected = true;
        }
        else if (!p2selected && p1selected)
        {
            GameManager.Instance.p2 = "Noob";
            selectText.gameObject.SetActive(false);
            p2selected = true;
            chooseScene();
        }
    }

    public void fighterKabal()
    {
        if (!p1selected)
        {
            GameManager.Instance.p1 = "Kabal";
            selectText.text = "Player 2";
            p1selected = true;
        }
        else if (!p2selected && p1selected)
        {
            GameManager.Instance.p2 = "Kabal";
            selectText.gameObject.SetActive(false);
            p2selected = true;
            chooseScene();

        }
    }

    public void chooseScene()
    {
        select.text = "Choose your Stage";
        characters.SetActive(false);
        scenarios.SetActive(true);
    }

    public void goToHell()
    {
        SceneManager.LoadScene("Hell");
    }

    public void goToPit()
    {
        SceneManager.LoadScene("Pit");
    }

    public void testYourMight()
    {
        SceneManager.LoadScene("TestYourMight");
    }
}
