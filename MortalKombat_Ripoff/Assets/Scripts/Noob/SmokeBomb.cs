﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeBomb : MonoBehaviour
{
    GameObject target;
    Transform targetPos;
    float speed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        targetPos = GameObject.Find("Kabal").gameObject.transform;
        Destroy(this.gameObject, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, targetPos.position, step);
        //this.gameObject.GetComponent<Rigidbody2D>().MovePosition(target.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Kabal")
        {
            collision.GetComponent<PlayerMovement>().freeze();
            
            Destroy(this.gameObject);
        }
    }
}
