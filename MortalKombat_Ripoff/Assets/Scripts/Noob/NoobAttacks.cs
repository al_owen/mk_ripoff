﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoobAttacks : MonoBehaviour
{
    public GameObject character;
    public GameObject special;
    public GameObject specialNoob;
    public Transform shootPlace;

    Animator _animator;
    int comboStatusS;
    int comboStatusN;
    int player;

    // Start is called before the first frame update
    void Start()
    {
        player = this.gameObject.GetComponent<PlayerMovement>().Player;
        _animator = character.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.isGameOver)
        {
            attack();
            block();
        }

    }

    private void block()
    {
        if (player == 1)
        {
            if (Input.GetKey(KeyCode.L))
            {
                _animator.SetBool("block", true);
            }
            else if (!Input.GetKey(KeyCode.S))
            {
                _animator.SetBool("block", false);
            }
        }
        else if (player == 2)
        {
            if (Input.GetKey(KeyCode.Keypad6))
            {
                _animator.SetBool("block", true);
            }
            else if (!Input.GetKey("down"))
            {
                _animator.SetBool("block", false);
            }
        }

    }

    private void attack()
    {
        if (player == 1)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                StartCoroutine(combo1("s"));
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                _animator.SetTrigger("upKick");
                StartCoroutine(combo("k"));
                StartCoroutine(combo1("k"));
            }
            if (Input.GetKeyDown(KeyCode.J))
            {
                _animator.SetTrigger("punchUp");
                StartCoroutine(combo("p"));
                StartCoroutine(combo1("p"));
            }
        }
        else if (player == 2)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                StartCoroutine(combo1("s"));
            }
            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                _animator.SetTrigger("upKick");
                StartCoroutine(combo("k"));
                StartCoroutine(combo1("k"));
            }
            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                _animator.SetTrigger("punchUp");
                StartCoroutine(combo("p"));
                StartCoroutine(combo1("p"));
            }
        }

    }

    //k p p k
    IEnumerator combo(string key)
    {
        if (key == "k")
        {
            if (comboStatusS == 0)
            {
                comboStatusS = 1;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusS == 1)
                {
                    comboStatusS = 0;
                }
            }
            else if (comboStatusS == 3)
            {
                comboStatusS = 4;
                _animator.SetTrigger("special");
                Instantiate(special, transform.position, Quaternion.identity);
            }
            else
            {
                comboStatusS = 0;
            }
        }
        else if (key == "p")
        {
            if (comboStatusS == 1)
            {
                comboStatusS = 2;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusS == 2)
                {
                    comboStatusS = 0;
                }
            }
            else if (comboStatusS == 2)
            {
                comboStatusS = 3;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusS == 3)
                {
                    comboStatusS = 0;
                }
            }
            else
            {
                comboStatusS = 0;
            }
        }
    }

    //s s p k
    IEnumerator combo1(string key)
    {
        if (key == "s")
        {
            if (comboStatusN == 0)
            {
                comboStatusN = 1;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusN == 1)
                {
                    comboStatusN = 0;
                }
            }
            else if (comboStatusN == 1)
            {
                comboStatusN = 2;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusN == 2)
                {
                    comboStatusN = 0;
                }
            }
            else
            {
                comboStatusN = 0;
            }
        }
        else if (key == "p")
        {
            if (comboStatusN == 2)
            {
                comboStatusN = 3;
                yield return new WaitForSeconds(0.4f);
                if (comboStatusN == 3)
                {
                    comboStatusN = 0;
                }
            }
        }
        else if (key == "k")
        {
            if (comboStatusN == 3)
            {
                comboStatusN = 4;
                _animator.SetTrigger("special");
                if (this.gameObject.GetComponent<PlayerMovement>().side == playerSide.right)
                {
                    Instantiate(specialNoob, shootPlace.position, Quaternion.identity).GetComponent<SpecialNoob>().right = true;
                }
                else if (this.gameObject.GetComponent<PlayerMovement>().side == playerSide.left)
                {
                    Instantiate(specialNoob, shootPlace.position, Quaternion.identity).GetComponent<SpecialNoob>().right = false;
                }

            }
        }
    }
}
