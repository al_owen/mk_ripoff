﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialNoob : MonoBehaviour
{
    public bool right;
    float _speed;

    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        if (right)
        {
            _speed = 5;
        }
        else if (!right)
        {
            _speed = -5;
            this.gameObject.transform.localScale *= new Vector2(-1, 1);
        }
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _rb.velocity = new Vector2(_speed, _rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<LifeManager>().animator.GetBool("block"))
            {
                collision.gameObject.GetComponent<LifeManager>().getHit(2);
            }
            else
            {
                collision.gameObject.GetComponent<LifeManager>().getHit(4);
            }
            Destroy(this.gameObject);
        }
        else if(collision.gameObject.name == "limits")
        {
            Destroy(this.gameObject);
        }
    }
}
