﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject p1;
    public GameObject p2;

    public Spawner p1Spawner;
    public Spawner p2Spawner;

    public Text timer;
    public Text winText;

    public GameObject[] winCounterP1, winCounterP2;
    private int round, p1Wins, p2Wins;

    public Slider p1Slider;
    public Slider p2Slider;

    private float countdown;
    private float _P1Life;
    private float _P2Life;
    public bool end;

    public void init()
    {
        p1 = p1Spawner.player;
        p2 = p2Spawner.player;
    }
    // Start is called before the first frame update
    void Start()
    {
        init();
        end = false;
        countdown = 61f;
        p1Slider.maxValue = p1.gameObject.GetComponent<LifeManager>().Life;
        updateLifeP1();
        p1.gameObject.GetComponent<LifeManager>().lifeEvent += updateLifeP1;

        p2Slider.maxValue = p2.gameObject.GetComponent<LifeManager>().Life;
        updateLifeP2();
        p2.gameObject.GetComponent<LifeManager>().lifeEvent += updateLifeP2;
    }

    // Update is called once per frame
    void Update()
    {
        if (countdown > 1 && !end)
        {
            countdown -= Time.deltaTime;
            timer.text = ("" + (int)countdown);
        }
        else if (countdown < 1 && !end)
        {
            countdown = 0;
            endOfRound();
        }
    }

    void updateLifeP1()
    {
        if (!end)
        {
            _P1Life = p1.gameObject.GetComponent<LifeManager>().Life;
            p1Slider.value = _P1Life;
            if (_P1Life < 1)
            {
                _P1Life = 0;
                endOfRound();
            }
        }
       
    }

    void updateLifeP2()
    {
        if (!end)
        {
            _P2Life = p2.gameObject.GetComponent<LifeManager>().Life;
            p2Slider.value = _P2Life;
            if (_P2Life < 1)
            {
                _P2Life = 0;
                endOfRound();
            }
        }
    }

    void endOfRound()
    {
        if (!end)
        {
            round++;
            GameManager.Instance.isGameOver = true;
            p1.gameObject.GetComponent<PlayerMovement>().freeze();
            p2.gameObject.GetComponent<PlayerMovement>().freeze();
            if (countdown < 1)
            {
                end = true;
                print("Time's up");
            }

            if (_P1Life > _P2Life)
            {

                if (p1Wins < 3)
                {

                    p1Wins++;
                    end = true;

                    winCounterP1[p1Wins - 1].SetActive(true);
                    p1.gameObject.GetComponentInChildren<Animator>().SetTrigger("victory");
                    winText.gameObject.SetActive(true);
                    winText.text = p1.gameObject.GetComponent<PlayerMovement>().name + " Wins";

                }
            }
            else if (_P1Life < _P2Life)
            {
                if (p1Wins < 3)
                {
                    end = true;
                    p2Wins++;
                    winCounterP2[p2Wins - 1].SetActive(true);
                    p2.gameObject.GetComponentInChildren<Animator>().SetTrigger("victory");
                    winText.gameObject.SetActive(true);
                    winText.text = p2.gameObject.GetComponent<PlayerMovement>().name + " Wins";
                }
            }
            else
            {
                winText.gameObject.SetActive(true);
                winText.text = "Draw";
            }

            if (p1Wins == 3 || p2Wins == 3)
            {
                Invoke("backToMenu", 3f);
            }
            else
            {
                Invoke("resetLevel", 2f);
            }
        }
    }

    void resetLevel()
    {
        GameManager.Instance.restart();
        countdown = 61f;
        end = false;

        p1.gameObject.GetComponent<LifeManager>().resetLife();
        p2.gameObject.GetComponent<LifeManager>().resetLife();
        updateLifeP1();
        updateLifeP2();
        p1Spawner.respawn();
        p2Spawner.respawn();

        winText.gameObject.SetActive(false);

    }

    void backToMenu()
    {
        GameManager.Instance.restart();
        SceneManager.LoadScene("Menu");
    }
}
